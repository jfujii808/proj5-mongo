import os
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

app = Flask(__name__)

# Creates the client
# DB_Port can be localhost or 127.0.0.1
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
#client = MongoClient("127.0.0.1",27017)
#client = MongoClient("172.17.03",58886)

# Connects client to database
db = client.tododb

@app.route('/')
def todo():
    # This gets the list of items in the database
    _items = db.tododb.find()
    # List of items
    items = [item for item in _items]

    return render_template('todo.html', items=items)
@app.route('/refresh', methods=['REFRESH'])
def todo():
    # This gets the list of items in the database
    _items = db.tododb.find()
    # List of items
    items = [item for item in _items]

    return render_template('todo.html', items=items)
# https://www.diffen.com/difference/GET-vs-POST-HTTP-Requests
@app.route('/new', methods=['POST'])
def new():
    item_doc = {
        'name': request.form['name'],
        'description': request.form['description']
    }
    db.tododb.insert_one(item_doc)

    return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
