# Project 5: Brevet time calculator with Ajax & MongoDB
Author: Jonathan Fujii
Contact Address: jfujii@oregon.edu
Description: This implements the RUSA ACP controle time calculator with flask, AJAX, and MongoDB.
User Instructions: 
Enter information via the provided forms and press submit.
To display the current times, hit Display
Any contextual information is provided by existing comments
